from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .models import Post
from .forms import PostForm
from django.views.generic import ListView, CreateView 
from django.urls import reverse_lazy 
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.core.files.storage import FileSystemStorage

import speech_recognition as sr
from os import path
import librosa
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import glob
from tqdm import tqdm_notebook
import warnings
warnings.simplefilter('ignore')
import IPython

def post_list(request):
	posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')
	return render(request, 'recognitionpost/post_list.html', {'posts': posts})

def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'recognitionpost/post_detail.html', {'post': post})

def post_new(request):
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            #post.author = request.user
            post.published_date = timezone.now()
            post.save()
            AUDIO_FILE_PATH = "/home/usman/code/project/site1/files/audio/"+str(request.FILES['files'])
            #new_audio = Post.objects.create(audio = request.FILES['audio'])
            # создаем экземпляр распознавателя и загружаем файл
            r = sr.Recognizer()
            with sr.AudioFile(AUDIO_FILE_PATH) as source:
                audio = r.record(source)  
            # распознаем при помощи google. 
            text = r.recognize_google(audio, language = 'ru')
            c_tipo= text.count('типо')
            c_vprincipe= text.count('в принципе')
            c_nu= text.count('ну')
            c_kakbi= text.count('как бы')
            c_koroche= text.count('короче')
            return JsonResponse({'text': text, 'words': {'типо': c_tipo, 'в принципе': c_vprincipe, 'ну': c_nu, 'как бы': c_kakbi, 'короче': c_koroche}})
        return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()
        return render(request, 'recognitionpost/post_edit.html', {'form': form})
